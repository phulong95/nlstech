<?php namespace Nlstech\Postall;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {
		return [
			'Nlstech\Postall\Components\PostList' => 'postList',
		];
	}

	public function registerSettings() {
	}
}
