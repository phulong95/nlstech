<?php namespace Nlstech\Postall\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\URL;
use NlsTech\Postall\Models\PostAll;
use System\Models\File;

class PostList extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'PostList Component',
			'description' => 'No description provided yet...',
		];
	}

	public $list = [];
	//public $image;
	public $imagePath = [];

	public function onRun() {
		$this->list = PostAll::all();
		$this->getListImage();

		dump($this->imageList);

	}

	public function getListImage() {
		$image = File::all();
		foreach ($image as $row) {
			if ($row->attachment_type === 'Nlstech\Slideshow\Models\SlideShow') {
				//return $row;
				$path = $row->disk_name;
				array_push($this->imagePath, URL::to('/') . "/" . "storage/app/uploads/public" . "/" . substr($path, 0, 3) . "/" . substr($path, 3, 3) . "/" . substr($path, 6, 3) . "/" . $row->disk_name);
			}
			//var_dump($row->disk_name);
		}
	}

	public function defineProperties() {
		return [
			'slug_component' => [
				'slug' => 'Scope value',
			],
		];
	}
}
