<?php namespace Nlstech\Postall\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechPostallInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_postall_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 200)->nullable();
            $table->text('content')->nullable();
            $table->integer('author_id')->nullable();
            $table->string('slug', 200);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_postall_info');
    }
}
