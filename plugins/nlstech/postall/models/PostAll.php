<?php namespace Nlstech\Postall\Models;

use Model;

/**
 * Model
 */
class PostAll extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'nlstech_postall_info';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $belongsTo = [
		'author' => 'Backend\Models\User',
	];

	public $attachOne = [
		'imageFeature' => 'System\Models\File',
	];
}
