<?php namespace Nlstech\Postall\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use NlsTech\Postall\Models\PostAll;

class PostAll extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
    }

    public function formAfterCreate(PostAll $post) {
    	$user = BackendAuth::getUser();
    	$post->author = $user->id;
    	$post->save();
    }
}
