<?php namespace Nlstech\Slideshow;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {
		return [
			'Nlstech\SlideShow\Components\ImageList' => 'imageList',
		];
	}

	public function registerSettings() {
	}
}
