<?php namespace Nlstech\Slideshow\Models;

use Model;

/**
 * Model
 */
class SlideShow extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'nlstech_slideshow_info';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $attachOne = [
		'fileImage' => 'System\Models\File',
	];
}
