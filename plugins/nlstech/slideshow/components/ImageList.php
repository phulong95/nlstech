<?php namespace Nlstech\SlideShow\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\URL;
use Nlstech\Slideshow\Models\SlideShow;
use System\Models\File;

class ImageList extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'Image list',
			'description' => 'Show list image',
		];
	}

	public $image;
	public $imagePath = [];

	public function onRun() {

		//var_dump($this->imagePath);
		$this->getListImage();
		//dump($this->imagePath);
	}

	public function getListImage() {
		$this->image = File::all();
		foreach ($this->image as $row) {
			if ($row->attachment_type === 'Nlstech\Slideshow\Models\SlideShow') {
				//return $row;
				$path = $row->disk_name;
				array_push($this->imagePath, URL::to('/') . "/" . "storage/app/uploads/public" . "/" . substr($path, 0, 3) . "/" . substr($path, 3, 3) . "/" . substr($path, 6, 3) . "/" . $row->disk_name);
			}
			//var_dump($row->disk_name);
		}
	}

	public function defineProperties() {
		return [];
	}
}
