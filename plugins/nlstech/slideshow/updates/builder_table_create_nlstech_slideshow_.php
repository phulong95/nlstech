<?php namespace Nlstech\Slideshow\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNlstechSlideshow extends Migration {
	public function up() {
		Schema::create('nlstech_slideshow_info', function ($table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name', 200)->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
		});
	}

	public function down() {
		Schema::dropIfExists('nlstech_slideshow_info');
	}
}
