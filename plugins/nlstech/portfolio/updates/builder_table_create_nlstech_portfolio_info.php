<?php namespace Nlstech\Portfolio\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechPortfolioInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_portfolio_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name_project', 200);
            $table->text('description_project');
            $table->string('title_meta', 200)->nullable();
            $table->string('description_meta', 200)->nullable();
            $table->string('slug', 200);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_portfolio_info');
    }
}
