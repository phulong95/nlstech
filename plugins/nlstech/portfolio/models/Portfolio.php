<?php namespace Nlstech\Portfolio\Models;

use Model;

/**
 * Model
 */
class Portfolio extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'nlstech_portfolio_info';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $attachOne = [
		'image_background' => 'System\Models\File',
	];
}
