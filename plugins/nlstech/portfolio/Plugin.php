<?php namespace Nlstech\Portfolio;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {
		return [
			'Nlstech\Portfolio\Components\ShowPortfolio' => 'showportfolio',
			'Nlstech\Portfolio\Components\ShowPortfolioDetail' => 'ShowPortfoliodetail',
		];
	}

	public function registerSettings() {
	}
}
