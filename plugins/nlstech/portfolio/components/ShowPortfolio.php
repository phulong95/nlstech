<?php namespace Nlstech\Portfolio\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\URL;
use NlsTech\Portfolio\Models\Portfolio;
use System\Models\File;

class ShowPortfolio extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'ShowPortfolio',
			'description' => 'Show Portfolio',
		];
	}

	public $list = [];
	public $imagePath = [];

	public function onRun() {
		$this->list = Portfolio::all();
		$this->getListImage();

		//dump($this->imagePath);
	}

	public function getListImage() {
		$image = File::all();
		foreach ($image as $row) {
			if ($row->attachment_type === 'Nlstech\Portfolio\Models\Portfolio') {
				//return $row;
				$path = $row->disk_name;
				array_push($this->imagePath, URL::to('/') . "/" . "storage/app/uploads/public" . "/" . substr($path, 0, 3) . "/" . substr($path, 3, 3) . "/" . substr($path, 6, 3) . "/" . $row->disk_name);
			}
			//var_dump($row->disk_name);
		}
	}

	public function defineProperties() {
		return [];
	}
}
