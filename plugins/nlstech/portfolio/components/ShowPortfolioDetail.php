<?php namespace Nlstech\Portfolio\Components;

use Cms\Classes\ComponentBase;
use NlsTech\Portfolio\Models\Portfolio;

class ShowPortfolioDetail extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'ShowPortfolioDetail',
			'description' => 'Show Portfolio Detail',
		];
	}

	public $list = [];

	public function onRun() {

		$slug = $this->param('slug');
		$this->list = Portfolio::where('slug', '=', $slug)->get();

		//dump($this->list[0]->name_project);
	}

	function onTest() {
		$this->page['result'] = input('value1') + input('value2');
	}

	function onSubmitContactForm() {
		$this->page['result'] = "<h1>FJSLDFJSJFLSJDF</h1>";
	}

	public function defineProperties() {
		return [
			'slug_component' => [
				'slug' => 'Scope value',
			],
		];
	}
}
