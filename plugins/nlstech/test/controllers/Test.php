<?php namespace Nlstech\Test\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Test extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nlstech.Test', 'main-menu-item', 'side-menu-item');
    }
}
