<?php namespace Nlstech\Test\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechTestInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_test_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 200);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_test_info');
    }
}
