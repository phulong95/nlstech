<?php namespace NlsTech\Post\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNlstechPostInfo2 extends Migration
{
    public function up()
    {
        Schema::table('nlstech_post_info', function($table)
        {
            $table->renameColumn('author', 'author_id');
        });
    }
    
    public function down()
    {
        Schema::table('nlstech_post_info', function($table)
        {
            $table->renameColumn('author_id', 'author');
        });
    }
}
