<?php namespace NlsTech\Post\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNlstechPostInfo extends Migration
{
    public function up()
    {
        Schema::table('nlstech_post_info', function($table)
        {
            $table->string('title', 191)->nullable()->change();
            $table->text('content')->nullable()->change();
            $table->integer('author')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nlstech_post_info', function($table)
        {
            $table->string('title', 191)->nullable(false)->change();
            $table->text('content')->nullable(false)->change();
            $table->integer('author')->nullable(false)->change();
        });
    }
}
