<?php namespace NlsTech\Post\Models;

use Model;

/**
 * Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'content'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nlstech_post_info';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'author' => 'Backend\Models\User'
    ];

    public $attachOne = [
        'feature_image' => 'System\Models\File'
    ];
}
