<?php namespace NlsTech\Post\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use NlsTech\Post\Models\Post;

class Posts extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('NlsTech.Post', 'post-menu');
    }

    public function formAfterCreate(Post $post) {
    	$user = BackendAuth::getUser();
    	$post->author = $user->id;
    	$post->save();
    }
}
