<?php namespace NlsTech\Skill\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechSkillInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_skill_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('skill');
            $table->smallInteger('level')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_skill_info');
    }
}
