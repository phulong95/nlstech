<?php namespace NlsTech\Skill\Models;

use Model;

/**
 * Model
 */
class Skill extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nlstech_skill_info';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
