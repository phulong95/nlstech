<?php namespace Nlstech\Whychooseuspost\Models;

use Model;

/**
 * Model
 */
class WhyChooseUsPost extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'nlstech_whychooseuspost_info';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $attachOne = [
		'feature_image' => 'System\Models\File',
	];
}
