<?php namespace Nlstech\WhyChooseUsPost\Components;

use Cms\Classes\ComponentBase;
use NlsTech\WhyChooseUsPost\Models\WhyChooseUsPost;

class ShowPostDetail extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'ShowPostDetail Component',
			'description' => 'No description provided yet...',
		];
	}

	public $list = [];

	public function onRun() {

		$slug = $this->param('slug');
		$this->list = WhyChooseUsPost::where('slug', '=', $slug)->get();

		dump($this->list[0]->title);
	}

	public function defineProperties() {
		return [
			'slug_component' => [
				'slug' => 'Scope value',
			],
		];
	}
}
