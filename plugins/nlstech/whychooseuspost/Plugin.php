<?php namespace Nlstech\Whychooseuspost;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {
		return [
			'Nlstech\WhyChooseUsPost\Components\ShowPost' => 'showpost',
			'Nlstech\WhyChooseUsPost\Components\ShowPostDetail' => 'showpostdetail',
		];
	}

	public function registerSettings() {
	}
}
