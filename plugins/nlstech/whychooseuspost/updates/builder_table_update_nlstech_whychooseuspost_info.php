<?php namespace Nlstech\Whychooseuspost\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNlstechWhychooseuspostInfo extends Migration
{
    public function up()
    {
        Schema::table('nlstech_whychooseuspost_info', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nlstech_whychooseuspost_info', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
