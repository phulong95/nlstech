<?php namespace Nlstech\Whychooseuspost\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechWhychooseuspostInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_whychooseuspost_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 200);
            $table->string('content', 200);
            $table->string('slug', 200);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_whychooseuspost_info');
    }
}
