<?php namespace Nlstech\Member\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\URL;
use NlsTech\Member\Models\Member;
use System\Models\File;

class ShowMember extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'ShowMember Component',
			'description' => 'No description provided yet...',
		];
	}

	public $list = [];
	public $imagePath = [];

	public function onRun() {
		$this->list = Member::all();
		$this->getListImage();

	}

	public function getListImage() {
		$image = File::all();
		foreach ($image as $row) {
			if ($row->attachment_type === 'Nlstech\Member\Models\Member') {
				//return $row;
				$path = $row->disk_name;
				array_push($this->imagePath, URL::to('/') . "/" . "storage/app/uploads/public" . "/" . substr($path, 0, 3) . "/" . substr($path, 3, 3) . "/" . substr($path, 6, 3) . "/" . $row->disk_name);
			}
			//var_dump($row->disk_name);
		}
	}

	public function defineProperties() {
		return [];
	}
}
