<?php namespace Nlstech\Member\Models;

use Model;

/**
 * Model
 */
class Member extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'nlstech_member_info';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $attachOne = [
		'feature_image' => 'System\Models\File',
	];
}
