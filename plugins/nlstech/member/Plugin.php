<?php namespace Nlstech\Member;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {
		return [
			'Nlstech\Member\Components\ShowMember' => 'showmember',
		];
	}

	public function registerSettings() {
	}
}
