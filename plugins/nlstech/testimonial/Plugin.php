<?php namespace Nlstech\Testimonial;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function registerComponents() {

		return [
			'Nlstech\Testimonial\Components\ShowTestimonial' => 'showtestimonial',
		];
	}

	public function registerSettings() {
		return [
			'settings' => [
				'label' => 'Testimonial Settings',
				'description' => 'Testimonial settings.',
				'category' => 'Testimonial',
				'icon' => 'icon-credit-card',
				'class' => 'Nlstech\Testimonial\Models\Settings',
				'order' => 500,
			],
		];
	}
}
