<?php namespace Nlstech\Testimonial\Components;

use Cms\Classes\ComponentBase;
use NlsTech\Testimonial\Models\Settings;
use NlsTech\Testimonial\Models\Testimonial;

class ShowTestimonial extends ComponentBase {
	public function componentDetails() {
		return [
			'name' => 'ShowTestimonial Component',
			'description' => 'No description provided yet...',
		];
	}

	public $list = [];
	public $feature_image;

	public function onRun() {
		$this->list = Testimonial::all();
		$this->feature_image = Settings::instance()->feature_image;
		// dump(Settings::instance()->feature_image);
	}

	public function defineProperties() {
		return [];
	}
}
