<?php namespace Nlstech\Testimonial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNlstechTestimonialInfo extends Migration
{
    public function up()
    {
        Schema::create('nlstech_testimonial_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 200);
            $table->text('content');
            $table->string('name_person', 200);
            $table->string('roll', 200);
            $table->string('link_web', 200);
            $table->string('title_meta', 200);
            $table->string('description_meta', 200);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nlstech_testimonial_info');
    }
}
