<?php namespace Nlstech\Testimonial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNlstechTestimonialInfo extends Migration
{
    public function up()
    {
        Schema::table('nlstech_testimonial_info', function($table)
        {
            $table->string('company_name', 200);
        });
    }
    
    public function down()
    {
        Schema::table('nlstech_testimonial_info', function($table)
        {
            $table->dropColumn('company_name');
        });
    }
}
