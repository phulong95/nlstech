<?php namespace Nlstech\Testimonial\Models;

use Model;

/**
 * Model
 */
class Testimonial extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nlstech_testimonial_info';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
