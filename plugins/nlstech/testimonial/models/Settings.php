<?php namespace Nlstech\Testimonial\Models;

use Model;

/**
 * Model
 */
class Settings extends Model {
	public $implement = ['System.Behaviors.SettingsModel'];

	// A unique code
	public $settingsCode = 'testimonial_settings';

	// Reference to field configuration
	public $settingsFields = 'fields.yaml';

}
