jQuery(function($) {
  	$('.sec-skills .pieProgress').asPieProgress({
    	namespace: 'pieProgress',
    	easing: 'linear',
    	speed: 40,
    	first: 0,
    	barsize: 15,
    	trackcolor:"#d8dcd6",
    	max: 100,
    	numberCallback: function(n) {
    		var number = Math.floor(n);
    		return number + '%';
    	}
  	});
  	$('.sec-skills .pieProgress').asPieProgress('start');
});